﻿using Android.Widget;

namespace AuditApp.Adapters
{
    internal class AreaListener : Java.Lang.Object, CompoundButton.IOnCheckedChangeListener
    {

        public System.Collections.Generic.Dictionary<int, bool> map;
        public int mPosotion;

        public AreaListener(System.Collections.Generic.Dictionary<int, bool> map, int position)
        {
            this.map = map;
            mPosotion = position;
        }

        public void OnCheckedChanged(CompoundButton buttonView, bool isChecked)
        {
            if (isChecked == true)
            {
                if (!map.ContainsKey(mPosotion))
                {
                    map.Add(mPosotion, true);
                }
            }
            else
            {
                map.Remove(mPosotion);
            }
        }
    }
    }
