﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;


namespace AuditApp.Models
{
    public class AreaModel
    {
        public string Id { get; set; }
        public string TextPrimary { get; set; }
        public string TextSecondary { get; set; }

    }

    public class SavedAudit
    {
     
            [JsonProperty("audit_id")]
            public string AuditId { get; set; }
            [JsonProperty("areas")]
            public string Areas { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }



    }
}