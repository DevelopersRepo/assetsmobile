﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using AndroidX.AppCompat.App;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.Toptoche.Searchablespinnerlibrary;
using Newtonsoft.Json;
using AuditApp.Data;
using static Android.Views.ViewGroup;
using Android.Graphics;
using Android.Views;
using System.Threading.Tasks;

namespace AuditApp
{
    [Activity(Label = "PerformAudit")]


    public class PerformAuditActivity : AppCompatActivity
    {
        private SearchableSpinner select_audit_site;
        private List<Data.AppSite> _areas;
        private int row_count;
        private const int SCAN_CODE_RESULT = 1010;
        private Button _scanButton;
        private int site_id;
        private TextView _submitt;
        private TextView _audit_title;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var _token = Intent.GetStringExtra("Token");
            var sites = Intent.GetStringExtra("Selected Sites");
            SetContentView(Resource.Layout.activity_perform_audit);

            select_audit_site = this.FindViewById<SearchableSpinner>(Resource.Id.select_audit_site);

            _areas = JsonConvert.DeserializeObject<List<Data.AppSite>>(sites);

            _scanButton = this.FindViewById<Button>(Resource.Id.btn_scan_asset);

            _scanButton.Click += _scanButton_Click;

            _submitt = this.FindViewById<TextView>(Resource.Id.in_submit_btn);

            _submitt.Click += submit_movement;

            _audit_title= this.FindViewById<TextView>(Resource.Id.audit_title);

            AndroidX.AppCompat.Widget.Toolbar toolbar = FindViewById<AndroidX.AppCompat.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            _audit_title.Text = Intent.GetStringExtra("audit_title");

            List<string> _sites = _areas.Select(x => x.Name).ToList();
            List<string> sites_array = new List<string>() { "___ Select Site ___" };

            sites_array.AddRange(_sites);

            ArrayAdapter sites_dapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleDropDownItem1Line, sites_array);
            select_audit_site.Adapter = sites_dapter;

            select_audit_site.ItemSelected += list_assets;

            //search_asset_box = FindViewById<SearchableSpinner>(Resource.Id.spinnerrr);
            // Create your application here
        }

        public async void list_assets(object sender, AdapterView.ItemSelectedEventArgs e)
        {

            try
            {
                ProgressDialog progress = new ProgressDialog(this);
                progress.SetTitle("Loading");
                progress.SetMessage("Please wait..");
                progress.Show();
                var selected_site = select_audit_site.SelectedItem.ToString();
                if (selected_site != "___ Select Site ___")
                {
                    site_id = _areas.Where(i => i.Name.ToString() == selected_site).Select(x => x.Id).First();

                    var audit_id = Intent.GetStringExtra("audit_id");
                    using (var conn = new Connect())
                    {
                        var token = FlexAppDatabase.GetTokens().FirstOrDefault();
                        object data_ = null;


                        var res = await conn.GetAsync(Defaults.AUDIT_SITE_ASSETS + "/" + site_id + "/" + audit_id, token.Token);
                        draw_table(res);
                    }
                }



                //var table = (TableLayout)FindViewById(Resource.Id.rvAuditAttributes);

                //ViewGroup parent = (ViewGroup)table;
                //if (parent != null)
                //{
                //    parent.RemoveAllViews();
                //}

                    //K?.removeAllViewsInLayout();

                //ProgressDialog progress = new ProgressDialog();
                //progress.SetTitle("Saving  Audit");
                //progress.SetMessage("Please wait..");
                //progress.Show();


               

                progress.Hide();
            }

            catch (Exception err)
            {

            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_logout)
            {

                FlexAppDatabase.ClearTokens();
                Intent LoginActivity = new Intent(this, typeof(LoginActivity));
                StartActivity(LoginActivity);
                /* return true;*/
            }
            if (id == Resource.Id.action_update)
            {
                var uri = Android.Net.Uri.Parse(Defaults.ROOT + Defaults.DOWNLOAD);
                var intent = new Intent(Intent.ActionView, uri);
                StartActivity(intent);
            }

            if (id == Resource.Id.action_go_to_web)
            {
                var uri = Android.Net.Uri.Parse(Defaults.ROOT);
                var intent = new Intent(Intent.ActionView, uri);
                StartActivity(intent);
            }

            return base.OnOptionsItemSelected(item);
        }

        public async void  submit_movement(object sender, EventArgs e)
        {
            try
            {
                ProgressDialog progress = new ProgressDialog(this);
                progress.SetTitle("Submit Audit");
                progress.SetMessage("Please wait..");
                progress.Show();
                var audit_id = Intent.GetStringExtra("audit_id");
                var token = FlexAppDatabase.GetTokens().FirstOrDefault();

                using (var conn = new Connect())
                {
                    var response = await conn.PostAsync(Defaults.SUBMIT_AUDIT, new { id = audit_id }, token.Token);

                    var intent = new Intent(this, typeof(MainActivity));

                    StartActivity(intent);

                }
                progress.Hide();
            }
            catch(Exception err)
            {

            }
        }

        public async void draw_table(string res)
        {
            var profileTable = (TableLayout)FindViewById(Resource.Id.rvAssetAuditTable);

                row_count = 0;

            profileTable.RemoveAllViews();
        //    for (int i = 0; i < count; i++)
        //    {
        //        View child = table.getChildAt(i);
        //        if (child instanceof TableRow) ((ViewGroup)child).removeAllViews();
        //}

        //var intent = new Intent(this, typeof(PerformAuditActivity));
        //intent.PutExtra("Selected Sites", JsonConvert.SerializeObject(selected_sites));
        //StartActivity(intent);


        var _assets = JsonConvert.DeserializeObject<Models.AssetProfile>(res);

                var assets_found = JsonConvert.SerializeObject(_assets.AuditAssets);
                var assets_found_details = JsonConvert.DeserializeObject<List<Models.AssetProfileAsset>>(assets_found);

            //addLine2("Assets Found");

            addRow("Asset", "Status");

            foreach (var asset_found in assets_found_details)
                {

                    addRow(asset_found.Name + " " + asset_found.Tag,"Found");

                }

                //addLine2("NotFound");

                var assets = JsonConvert.SerializeObject(_assets.Assets);
                var assets_details = JsonConvert.DeserializeObject<List<Models.AssetProfileAsset>>(assets);
                foreach (var asset in assets_details)
                {

                    addRow(asset.Name + " " + asset.Tag,"Not Found");

                }

                //addLine2("Misplaced");


                var assets_misplaced = JsonConvert.SerializeObject(_assets.MisplacedAssets);
                var assets_misplaced_details = JsonConvert.DeserializeObject<List<Models.AssetProfileAsset>>(assets_misplaced);

                foreach (var asset_misplaced in assets_misplaced_details)
                {

                    addRow(asset_misplaced.Name + " " + asset_misplaced.Tag,"Misplaced");

                }
            
        }

      private void _scanButton_Click(object sender, EventArgs e)
        {
            var selected_site = select_audit_site.SelectedItem.ToString();
            if (selected_site != "___ Select Site ___")
            {

                Task task = new Task(() =>
                {
                    Intent activity1 = new Intent(this, typeof(ScanCodeActivity));
                    StartActivityForResult(activity1, 0);
                });

                task.Start();

            }

            else
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                alert.SetTitle("Alert");
                alert.SetMessage("Please select Site First");
                alert.SetPositiveButton("Ok", (senderAlert, args) => { });
                Dialog dialog = alert.Create();
                dialog.Show();
            }
        }

        void addRow(string label, string details)
        {

            try
            {

                var profileTable = (TableLayout)FindViewById(Resource.Id.rvAssetAuditTable);


                TableLayout.LayoutParams rowLayoutParams = new TableLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

                TableRow.LayoutParams columnLayoutParams2 = new TableRow.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

                TextView column1 = new TextView(this);

                //Use TableRow
                column1.LayoutParameters = columnLayoutParams2;
                column1.SetTextSize(Android.Util.ComplexUnitType.Px, (float)25.0);
                column1.SetTextColor(Color.Black);
                column1.SetPadding(5, 5, 5, 5);

                View line = new View(this);

                line.SetMinimumHeight(2);


                TextView column0 = new TextView(this);
                column0.LayoutParameters = columnLayoutParams2;

                column0.SetTextSize(Android.Util.ComplexUnitType.Px, (float)25.0);
                column0.SetTextColor(Color.Black);
                column0.SetPadding(5, 5, 5, 5);

                TextView column2 = new TextView(this);

                //Use TableRow
                column2.LayoutParameters = columnLayoutParams2;

                column2.SetTextColor(Color.Black);
                column2.SetTextSize(Android.Util.ComplexUnitType.Px, (float)25.0);
                column2.SetPadding(5, 5, 5, 5);
                TableRow tr = new TableRow(this);

                //Use TableLayout
                tr.LayoutParameters = rowLayoutParams;

                if (row_count % 2 == 0)
                {
                    tr.SetBackgroundColor(Android.Graphics.Color.ParseColor("#eaeaea"));
                }

                else
                {
                    tr.SetBackgroundColor(Android.Graphics.Color.ParseColor("#f7f7f7"));
                }

                column2.Text = details;
                column1.Text = label;
                column0.Text = " ";
                tr.AddView(column0);
                tr.AddView(column1);
                tr.AddView(column2);
                profileTable.AddView(tr);
                //profileTable.AddView(line);

                row_count++;
            }

#pragma warning disable CS0168 // The variable 'err' is declared but never used
            catch (Exception err)
#pragma warning restore CS0168 // The variable 'err' is declared but never used
            {

            }
        }

        void addLine(string title)
        {


            var profileTable = (TableLayout)FindViewById(Resource.Id.rvAssetAuditTable);


            TableLayout.LayoutParams rowLayoutParams = new TableLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

            TableRow.LayoutParams columnLayoutParams2 = new TableRow.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

            TextView column1 = new TextView(this);

            column1.LayoutParameters = columnLayoutParams2;

            column1.SetTextSize(Android.Util.ComplexUnitType.Px, (float)30.0);
            column1.SetTextColor(Color.Black);
            column1.SetPadding(30, 30, 30, 30);
            column1.SetBackgroundColor(Android.Graphics.Color.ParseColor("#75b4ef"));


            View line = new View(this);

            line.SetBackgroundColor(Android.Graphics.Color.ParseColor("#FF909090"));
            TableRow tr = new TableRow(this);

            //Use TableLayout
            tr.LayoutParameters = rowLayoutParams;
            column1.Text = title;
            profileTable.AddView(column1);

        }

        void addLine2(string title)
        {
            var profileTable = (TableLayout)FindViewById(Resource.Id.rvAssetAuditTable);


            TableLayout.LayoutParams rowLayoutParams = new TableLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

            TableRow.LayoutParams columnLayoutParams2 = new TableRow.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);

            TextView column1 = new TextView(this);

            column1.LayoutParameters = columnLayoutParams2;

            column1.SetTextSize(Android.Util.ComplexUnitType.Px, (float)30.0);
            column1.SetTextColor(Color.Black);
            column1.SetPadding(5, 5, 5, 5);
            column1.SetBackgroundColor(Android.Graphics.Color.ParseColor("#FF909090"));

            column1.Text = title;
            profileTable.AddView(column1);

        }



        string[] getBranchArea(string site)
        {
            string[] BranchArea = site.Split('#');

            return BranchArea;
        }

        protected async override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            try
            {
                ProgressDialog progress = new ProgressDialog(this);
                progress.SetTitle("Loading");
                progress.SetMessage("Please wait..");
                progress.Show();

                base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 0 && data != null)
            {
                var codes = data.GetStringExtra("barcode");

                List<string> code_list = JsonConvert.DeserializeObject<List<string>>(codes);


                var audit_id = Intent.GetStringExtra("audit_id");
                var token = FlexAppDatabase.GetTokens().FirstOrDefault();

                using (var conn = new Connect())
                {
                    var response = await conn.PostAsync(Defaults.ADD_AUDITS_ASSET, new { tag = code_list, audit_id = audit_id, site_id = site_id }, token.Token);
                        if (response != "")
                        {
                            draw_table(response);
                        }

                        else
                        {
                            Toast.MakeText(Application.Context, "Asset not found", ToastLength.Long).Show();

                        }


                    }


                



                //_assetSearchBar.Text = data.GetStringExtra("barcode");

                //progress.SetTitle("Search Asset");
                //progress.SetMessage("Please wait...");
                //progress.SetCancelable(false);
                //progress.Show();
                //SearchTag(data.GetStringExtra("barcode"));
            }

                progress.Hide();
            }

            catch (Exception err)
            {

            }
        }



    }



}

