﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuditApp.Holders  
{
    public class AreaViewHolder : RecyclerView.ViewHolder
    {
        public TextView arl;
        public TextView ars;
        public CheckBox chkbx;

        public AreaViewHolder(View view) : base(view)
        {
            arl = view.FindViewById<TextView>(Resource.Id.areaLocation);
            ars = view.FindViewById<TextView>(Resource.Id.areaSite);
            chkbx = view.FindViewById<CheckBox>(Resource.Id.area_select_checkbox);

            TextPrimary = arl;
            TextSecondary = ars;
            checkbox = chkbx;

        }

        public TextView TextPrimary { get; set; }
        public TextView TextSecondary { get; set; }

        public CheckBox checkbox { get; set; }




    }
}