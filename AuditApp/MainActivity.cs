﻿ using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Snackbar;
using AuditApp.FlexControls;
using AuditApp.Adapters;
using Button = Android.Widget.Button;
using AndroidX.RecyclerView.Widget;
using AuditApp.Models;
using AuditApp.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using Android.Content;
using System.Linq;
using Google.Android.Material.TextField;
using System.Globalization;
using System.Net;
using Android.Widget;

namespace AuditApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class MainActivity : AppCompatActivity,DatePickerDialog.IOnDateSetListener
    {
        private AreasRecyclerViewAdapter<Models.AreaModel, Holders.AreaViewHolder> areasRecyclerViewAdapter;
        private RecyclerView areasRecyclerView;
        private RecyclerView.LayoutManager rvLayoutManager;
        public NoSwipePager main_pager;
        public FlexPagerAdapter pagerAdapter;
        private TextInputEditText title;
        private TextInputEditText description;
        internal string _token;
        Button audit_start;
        Button audit_end;
        Button audit_next;
        private int year;
        private int date;
        private  int month;
        public string start_end = "";

        public List<AreaSite> selected_sites;

        public Context Context { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            AndroidX.AppCompat.Widget.Toolbar toolbar = FindViewById<AndroidX.AppCompat.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            title = FindViewById<TextInputEditText>(Resource.Id.input_title);
            description = FindViewById<TextInputEditText>(Resource.Id.input_description);

            audit_start = FindViewById<Button>(Resource.Id.audit_start);
            String myDate = DateTime.Now.ToString();
            audit_start.Click += Audit_start_Click;

            audit_end = FindViewById<Button>(Resource.Id.audit_end);
            audit_next = FindViewById<Button>(Resource.Id.audit_next);  
            audit_end.Click += Audit_end_Click;
            audit_next.Click += NextAuditAction;
            areasRecyclerView = FindViewById<RecyclerView>(Resource.Id.areas_recycler_view);

            selected_sites = new List<AreaSite>();

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;
            CheckAudit();
            LoadAreas();
        }


        private async void CheckAudit()
        {
           
            try
            {
                using (var conn = new Connect())
                {
                    var token = FlexAppDatabase.GetTokens().FirstOrDefault();


                    var response = await conn.GetAsync(Defaults.SAVED_AUDITS, token.Token);

                    if (response != "")
                    {
                        
                        var _response = JsonConvert.DeserializeObject<SavedAudit>(response);
                        //var _areas_string = JsonConvert.SerializeObject(_response.Areas);
                        var intent = new Intent(this, typeof(PerformAuditActivity));
                        intent.PutExtra("audit_id", _response.AuditId);
                        intent.PutExtra("Selected Sites", _response.Areas);
                        intent.PutExtra("audit_title", _response.Title);
                        StartActivity(intent);
                    }

                    else
                    {
                        Toast.MakeText(Application.Context, "No incomplete audit found", ToastLength.Long).Show();

                    }

                    


                }
            }

            catch (Exception err)
            {

            }



            //progress.Hide();

        }

        private void Audit_start_Click(object sender, EventArgs e)
        {
            start_end = "start";
            int DatePickerDialogId = 1; //user to indentify another dialog
            ShowDialog(DatePickerDialogId);
        }

        private void Audit_end_Click(object sender, EventArgs e)
        {
            start_end = "end";
            int DatePickerDialogId = 1; //user to indentify another dialog
            ShowDialog(DatePickerDialogId);
        }

        protected override Dialog OnCreateDialog(int id)
        {
            DateTime currently = DateTime.Now;
            if (id == 1)
            {
                return new DatePickerDialog(this, this, currently.Year, currently.Month - 1, currently.Day);
            }
            return null;
        }

        public bool CheckForInternetConnection(int timeoutMs = 10000, string url = null)
        {
            try
            {
                url ??= CultureInfo.InstalledUICulture switch
                {
                    { Name: var n } when n.StartsWith("fa") => // Iran
                        "http://www.aparat.com",
                    { Name: var n } when n.StartsWith("zh") => // China
                        "http://www.baidu.com",
                    _ =>
                        "http://www.gstatic.com/generate_204",
                };

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Timeout = timeoutMs;
                using (var response = (HttpWebResponse)request.GetResponse())
                    return true;
            }
            catch
            {
                return false;
            }
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_logout)
            {

                FlexAppDatabase.ClearTokens();
                Intent LoginActivity = new Intent(this, typeof(LoginActivity));
                StartActivity(LoginActivity);
                /* return true;*/
            }
            if (id == Resource.Id.action_update)
            {
                var uri = Android.Net.Uri.Parse(Defaults.ROOT + Defaults.DOWNLOAD);
                var intent = new Intent(Intent.ActionView, uri);
                StartActivity(intent);
            }

            if (id == Resource.Id.action_go_to_web)
            {
                var uri = Android.Net.Uri.Parse(Defaults.ROOT);
                var intent = new Intent(Intent.ActionView, uri);
                StartActivity(intent);
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void OnDateSet(Android.Widget.DatePicker view, int year, int month, int dayOfMonth)
        {
            this.year = year;
            this.month = month;
            this.date = dayOfMonth;
            if (start_end == "start")
            {
                audit_start.Text = "Date: " + date + "/" + month + "/" + year;
            }
            if (start_end == "end")
            {
                audit_end.Text = "Date: " + date + "/" + month + "/" + year;
            }
            
            
        }

        private async void LoadAreas( )
        {
            try
            {

                ProgressDialog progress = new ProgressDialog(this);
                progress.SetTitle("Loading Sites");
                progress.SetMessage("Please wait..");
                progress.Show();
                var token = FlexAppDatabase.GetTokens().FirstOrDefault();

                List<Models.AreaModel> _areas = new List<Models.AreaModel>();

                using (var conn = new Connect())
                {
                    var area_result = await conn.GetAsync(Defaults.GET_AUDIT_SITES, token.Token);

                    var assets = JsonConvert.DeserializeObject<List<AreaSite>>(area_result).Select(x => new { Id = x.SiteId, TextPrimary = x.Branch, TextSecondary = x.Site }).ToList();

                    var _assets_string = JsonConvert.SerializeObject(assets);
                    _areas = JsonConvert.DeserializeObject<List<Models.AreaModel>>(_assets_string);
                }

                areasRecyclerViewAdapter = new AreasRecyclerViewAdapter<Models.AreaModel, Holders.AreaViewHolder>(this.Context, _areas, (thing, holder, view) =>
                {
                    holder.TextPrimary.Text = thing.TextPrimary;
                    holder.TextSecondary.Text = thing.TextSecondary;
                    holder.checkbox.Id = Int32.Parse(thing.Id);


                    holder.checkbox.SetOnCheckedChangeListener(null);



                    holder.checkbox.Click += delegate (object sender, EventArgs e)
                    {

                        holder.checkbox.SetOnCheckedChangeListener(null);


                        area_selected(thing.Id, thing.TextPrimary, thing.TextSecondary, holder.checkbox.Checked);

                    };




                }, Resource.Layout.area_select_view);

                rvLayoutManager = new LinearLayoutManager(this.Context);


                areasRecyclerView.SetAdapter(areasRecyclerViewAdapter);
                areasRecyclerView.SetLayoutManager(rvLayoutManager);
                progress.Hide();
            }

            catch(Exception err) { }
        }


        private void area_selected(string id, string branch, string site,bool cheched)
        {

            
            var elected = selected_sites.Where(i => i.SiteId.ToString() == id).ToList();
            if (!cheched)
            {
                selected_sites.Remove(selected_sites.Where(i => i.SiteId.ToString() == id).First());

            }

            else
            {
                selected_sites.Add(new AreaSite { SiteId = Int32.Parse(id), Branch = branch, Site = site });

            }

           
        }

        private async void NextAuditAction(object sender, EventArgs e)
        {
            

            var _title = title.Text;
            var _description = description.Text;

            ProgressDialog progress = new ProgressDialog(this);
            progress.SetTitle("Saving  Audit");
            progress.SetMessage("Please wait..");
            progress.Show();
            var sites_ids = selected_sites.Select(x => x.SiteId).ToList();
            string sites = JsonConvert.SerializeObject(sites_ids);
            if (_title != "" && _description != "" && audit_start.Text != "Start" && audit_end.Text != "End" && sites_ids.Count()>0)
            {
                try
                {
                    
                    using (var conn = new Connect())
                    {
                        var token = FlexAppDatabase.GetTokens().FirstOrDefault();
                        object data_ = null;


                        data_ = new { title = _title, description = _description, start = audit_start.Text, end = audit_end.Text, sites = sites };

                        //data_ = new { title = "Title", description = "Description", start = "12/12/2021", end = "12/01/2022", sites = sites };

                        var res = await conn.PostAsync(Defaults.SAVE_AUDIT, data_, token.Token);

                        var intent = new Intent(this, typeof(PerformAuditActivity));
                        intent.PutExtra("Selected Sites", JsonConvert.SerializeObject(selected_sites));
                        intent.PutExtra("audit_id", res);
                        intent.PutExtra("audit_title", _title);
                        StartActivity(intent);


                    }
                }

                catch (Exception err)
                {

                }

            }

            else
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                alert.SetTitle("Alert");
                alert.SetMessage("Form Incopmlete");
                alert.SetPositiveButton("Ok", (senderAlert, args) => { });
                Dialog dialog = alert.Create();
                dialog.Show();
            }

            progress.Hide();

        }

    }
}
