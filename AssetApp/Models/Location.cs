﻿using Newtonsoft.Json;

namespace AssetApp.Models
{
    public class Location
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }


    }
}