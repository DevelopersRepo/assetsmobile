﻿namespace AssetApp.Models
{
    public class PlacementObject
    {

        public int asset_id { get; set; }
        public string LocationName { get; set; }
        public int location_id { get; set; }
        public string current_location_name { get; set; }
        public string current_location_id { get; set; }
    }
}