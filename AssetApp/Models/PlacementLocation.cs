﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AssetApp.Models
{
    public class PlacementLocation
    {
        [JsonProperty("sites")]
        public List<Location> Sites { get; set; }
        [JsonProperty("current_site")]
        public string CurrentSite { get; set; }
        [JsonProperty("current_site_name")]
        public string CurrrentSiteName { get; set; }
    }

}