using Newtonsoft.Json;
#pragma warning disable CS0105 // The using directive for 'System' appeared previously in this namespace
#pragma warning restore CS0105 // The using directive for 'System' appeared previously in this namespace
#pragma warning disable CS0105 // The using directive for 'System.Collections.Generic' appeared previously in this namespace
#pragma warning restore CS0105 // The using directive for 'System.Collections.Generic' appeared previously in this namespace
#pragma warning disable CS0105 // The using directive for 'System.Linq' appeared previously in this namespace
#pragma warning restore CS0105 // The using directive for 'System.Linq' appeared previously in this namespace
#pragma warning disable CS0105 // The using directive for 'System.Text' appeared previously in this namespace
#pragma warning restore CS0105 // The using directive for 'System.Text' appeared previously in this namespace

namespace AssetApp.Models
{
    public class Asset
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
        public decimal Price { get; set; }
    }

}