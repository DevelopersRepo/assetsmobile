﻿namespace AssetApp.Models
{
    public class Incidence
    {

        public int asset_id { get; set; }
        public string asset_name { get; set; }
        public string occurence_date { get; set; }
        public string title { get; set; }
        public string description { get; set; }

    }
}