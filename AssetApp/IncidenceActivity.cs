﻿using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using AssetApp.Data;
using AssetApp.Models;

namespace AssetApp
{
    [Activity(Label = "IncidenceActivity")]
    public class IncidenceActivity : Activity, DatePickerDialog.IOnDateSetListener
    {
        public string _token;
        public int asset_id;
        public string name;
        public int year;
        public int month;
        public int date;

        public string tag;
        Button occurence_input;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            _token = Intent.GetStringExtra("Token");
            asset_id = Intent.GetIntExtra("asset_id", 0);
            name = Intent.GetStringExtra("asset_name");
            tag = Intent.GetStringExtra("asset_tag");
            SetContentView(Resource.Layout.activity_incidence_reporting);


            occurence_input = FindViewById<Button>(Resource.Id.occurence_input);
            String myDate = DateTime.Now.ToString();
            occurence_input.Text = myDate;
            occurence_input.Click += Occurence_input_Click;

            EditText asset_tag_label = FindViewById<EditText>(Resource.Id.tag_input);
            asset_tag_label.Text = tag;

            EditText asset_name_label = FindViewById<EditText>(Resource.Id.name_input);
            asset_name_label.Text = name;
            Button submit_incidence = FindViewById<Button>(Resource.Id.submit);
            submit_incidence.Click += send_data;
            // Create your applicatioOnCreateDialogn here
        }

        private void Occurence_input_Click(object sender, EventArgs e)
        {
            int DatePickerDialogId = 1;//user to indentify another dialog
            ShowDialog(DatePickerDialogId);
        }

#pragma warning disable CS0672 // Member 'IncidenceActivity.OnCreateDialog(int)' overrides obsolete member 'Activity.OnCreateDialog(int)'. Add the Obsolete attribute to 'IncidenceActivity.OnCreateDialog(int)'.
        protected override Dialog OnCreateDialog(int id)
#pragma warning restore CS0672 // Member 'IncidenceActivity.OnCreateDialog(int)' overrides obsolete member 'Activity.OnCreateDialog(int)'. Add the Obsolete attribute to 'IncidenceActivity.OnCreateDialog(int)'.
        {

            DateTime currently = DateTime.Now;
            if (id == 1)
            {
                return new DatePickerDialog(this, this, currently.Year, currently.Month - 1, currently.Day);
            }
            return null;
        }

        private async void send_data(object sender, EventArgs e)
        {
            EditText title = FindViewById<EditText>(Resource.Id.incidence_title_input);
            EditText description = FindViewById<EditText>(Resource.Id.description_input);

            ProgressDialog progress2 = new ProgressDialog(this);
            progress2.SetTitle("Saving");
            progress2.SetMessage("Please wait...");

            using (var conn = new Connect())
            {
                try
                {
                    Incidence incidenceModel = new Incidence
                    {
                        asset_id = asset_id,
                        asset_name = name,
                        title = title.Text,
                        occurence_date = occurence_input.Text,
                        description = description.Text
                    };
                    progress2.Show();

                    var token = FlexAppDatabase.GetTokens().FirstOrDefault();
                    var response = await conn.PostAsync(Defaults.SUBMIT_INCIDENCE_WITH_ASSET, incidenceModel, token.Token);

                    Intent MainActivity = new Intent(Application.Context, typeof(MainActivity));
                    StartActivity(MainActivity);

                    Toast.MakeText(Application.Context, "You have successful report an incidence", ToastLength.Long).Show();

                    progress2.Hide();
                }
#pragma warning disable CS0168 // The variable 'err' is declared but never used
                catch (Exception err)
#pragma warning restore CS0168 // The variable 'err' is declared but never used
                {
                    progress2.Hide();
                    Toast.MakeText(Application.Context, "Fail to submit", ToastLength.Long).Show();

                }
            }
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            this.year = year;
            this.month = month;
            this.date = dayOfMonth;
            occurence_input.Text = date + "/" + month + "/" + year;

        }
    }
}