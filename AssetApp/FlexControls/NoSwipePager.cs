﻿using Android.Content;
using Android.Util;
using Android.Views;
using AndroidX.ViewPager.Widget;

namespace AssetApp.FlexControls
{

    public class NoSwipePager : ViewPager
    {
        public NoSwipePager(Context _context) : base(_context)
        {

        }

        public NoSwipePager(Context _context, IAttributeSet _attrs) : base(_context, _attrs)
        {

        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            return false;
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev)
        {
            return false;
        }
    }
}