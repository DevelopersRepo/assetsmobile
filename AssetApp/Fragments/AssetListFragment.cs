﻿using Android.OS;
using Android.Views;
using SupportFragment = AndroidX.Fragment.App.Fragment;
namespace AssetApp.Fragments
{
    public class AssetListFragment : SupportFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.fragment_asset_list, container, false);

            return view;
        }
    }
}