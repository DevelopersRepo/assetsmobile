using System.Collections.Generic;
#pragma warning disable CS0105 // The using directive for 'Android.App' appeared previously in this namespace
#pragma warning restore CS0105 // The using directive for 'Android.App' appeared previously in this namespace
using Newtonsoft.Json;

namespace AssetApp.Data
{

    public class MasterData
    {
        [JsonProperty("categories")]
        public List<AppCategory> Categories { get; set; }
        [JsonProperty("category_attributes")]
        public List<AppCategoryAttribute> CategoryAttributes { get; set; }
        [JsonProperty("assets")]
        public List<AppAsset> Assets { get; set; }
    }
}