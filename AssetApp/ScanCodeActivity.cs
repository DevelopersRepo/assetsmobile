﻿
//using Android;
//using Android.App;
//using Android.Content;
//using Android.Content.PM;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.Support.V4;
//using AndroidX.Core.App;
//using AndroidX.Core.Content;
//using ZXing.Mobile;
//using System.Threading.Tasks;
//using Android.Views.Animations;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Xamarin.Essentials;
//using Android.Graphics;

//namespace AssetApp
//{
//    [Activity(Label = "Scan Code")]
//    public class ScanCodeActivity : Activity
//    {

//        View zxingOverlay;
//        MobileBarcodeScanner scanner;
//        MobileBarcodeScanningOptions mbs;
//        Button flashlight;
//        protected override void OnCreate(Bundle savedInstanceState)
//        {
//            base.OnCreate(savedInstanceState);

//            // Create your application here
//            //SetContentView(Resource.Layout.ZxingOverlay);


//            scanner = new MobileBarcodeScanner();
//            Task t = new Task(AutoScan);
//            t.Start();



//        }

//        async void AutoScan()
//        {
//            scanner.UseCustomOverlay = true;
//            zxingOverlay = LayoutInflater.FromContext(this).Inflate(Resource.Layout.ZxingOverlay, null);

//            ImageView forecastImage = zxingOverlay.FindViewById<ImageView>(Resource.Id.lightbtn);


//            ImageView ivScanning = zxingOverlay.FindViewById<ImageView>(Resource.Id.ivScanning);
//            //flashlight = zxingOverlay.FindViewById<Button>(Resource.Id.lightbtn);
//            forecastImage.Click += (s, e) =>
//            {
//                if (scanner != null)
//                {
//                    scanner.ToggleTorch();

//                    if (scanner.IsTorchOn)
//                    {

//                        forecastImage.SetColorFilter(Color.LightBlue);
//                    }
//                    else
//                    {

//                        forecastImage.SetColorFilter(Color.ParseColor("#a9e6ff"));
//                    }
//                }
//            };


//            zxingOverlay.Measure(MeasureSpecMode.Unspecified.GetHashCode(), MeasureSpecMode.Unspecified.GetHashCode());
//            int width = zxingOverlay.MeasuredWidth;
//            int height = zxingOverlay.MeasuredHeight;

//            Animation verticalAnimation = new TranslateAnimation(0, 0, 0, height);
//            verticalAnimation.Duration = 3000; 
//            verticalAnimation.RepeatCount = Animation.Infinite; 

//            ivScanning.Animation = verticalAnimation;
//            verticalAnimation.StartNow();

//            scanner.CustomOverlay = zxingOverlay;
//            mbs = MobileBarcodeScanningOptions.Default;
//            mbs.AssumeGS1 = true;
//            mbs.AutoRotate = true;
//            mbs.DisableAutofocus = false;
//            mbs.PureBarcode = true;
//            mbs.TryInverted = true;
//            mbs.TryHarder = true;
//            mbs.UseCode39ExtendedMode = true;
//            mbs.UseFrontCameraIfAvailable = false;
//            mbs.UseNativeScanning = true;
//            mbs.InitialDelayBeforeAnalyzingFrames = 1000;



//            var result = await scanner.Scan(this, mbs);
//            HandleScanResult(result);

//        }


//        public void HandleScanResult(ZXing.Result result)
//        {
//            if (result != null && !string.IsNullOrEmpty(result.Text))
//            {
//                if (result.Text != null && result.Text.Trim().Length > 5)
//                {
//                    this.RunOnUiThread(() => { Toast.MakeText(Application.Context, result.Text, ToastLength.Short).Show(); });


//                    try
//                    {
//                        // Use default vibration length
//                        Vibration.Vibrate();

//                        // Or use specified time
//                        var duration = TimeSpan.FromSeconds(0.4);
//                        Vibration.Vibrate(duration);
//                    }
//                    catch (FeatureNotSupportedException ex)
//                    {
//                        // Feature not supported on device
//                    }
//                    catch (Exception ex)
//                    {
//                        // Other error has occurred.
//                    }


//                }
//            }

//            Intent intent = new Intent();

//            intent.PutExtra("barcode", result.Text);

//            SetResult(Result.Ok, intent);
//            Finish();
//            scanner.Cancel();


//        }




//    }


//}


using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views.Animations;
using Android.Widget;
using AndroidX.Core.App;
using AndroidX.Core.Content;
using EDMTDev.ZXingXamarinAndroid;


namespace AssetApp
{
    [Activity(Label = "Scan Code")]
    public class ScanCodeActivity : Activity, IResultHandler
    {
        private const int PERMISSIONS_REQUEST_CAMERA = 100;
        private ZXingScannerView scanView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.activity_scan_code);

            scanView = FindViewById<ZXingScannerView>(Resource.Id.zxScan);

            scanView.SetAutoFocus(true);

            scanView.SetShouldScaleToFill(true);

            List<ZXing.BarcodeFormat> formats = new List<ZXing.BarcodeFormat>();
            formats.Add(ZXing.BarcodeFormat.CODE_128);


            scanView.SetFormats(formats);
            scanView.SetLaserEnabled(true);
            scanView.SetBorderColor(0);
            scanView.SetMaskColor(0);
            scanView.SetLaserEnabled(false);


            RelativeLayout layout = this.FindViewById<RelativeLayout>(Resource.Id.rlscan);
            ImageView ivScanning = this.FindViewById<ImageView>(Resource.Id.ivScanning);

            int height = scanView.Height;

            Animation verticalAnimation = new TranslateAnimation(0, 0, 0, 300);
            verticalAnimation.Duration = 3000;
            verticalAnimation.RepeatCount = Animation.Infinite;
            ivScanning.Animation = verticalAnimation;
            verticalAnimation.StartNow();

            Task t = new Task(AutoScan);
            t.Start();

            //View  zxingOverlay = FindViewById < View > (Resource.Layout.ZxingOverlay);

            //scanView.AddView(zxingOverlay);




            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) != Permission.Granted)
            {

                ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera }, PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                scanView.SetResultHandler(this);
                scanView.StartCamera();
            }

        }

        void AutoScan()
        {
            var i = 1;
            while (i > 0)
            {
                //scanView.StartCamera();
                scanView.SetAutoFocus(true);
                int sleepTime = 2000; // in mills
                Task.Delay(sleepTime).Wait();

                scanView.SetAutoFocus(false);
                //scanView.StopCamera();

            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            switch (requestCode)
            {
                case PERMISSIONS_REQUEST_CAMERA:
                    {
                        if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                        {
                            scanView.SetResultHandler(this);
                            scanView.StartCamera();
                        }
                        else
                        {
                            Intent intent = new Intent();

                            intent.PutExtra("barcode", string.Empty);

                            SetResult(Result.Ok, intent);
                            Finish();
                        }
                    }

                    break;
                default:
                    break;
            }
        }

        protected override void OnDestroy()
        {
            scanView.StopCamera();
            base.OnDestroy();
        }

        public void HandleResult(ZXing.Result rawResult)
        {
            Intent intent = new Intent();

            intent.PutExtra("barcode", rawResult.Text);

            SetResult(Result.Ok, intent);

            Finish();
        }
    }


}